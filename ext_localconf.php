<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper::class] = [
        'className' => \VITD\FixImageviewhelper\Xclass\ViewHelpers\ImageViewHelper::class
    ];
})('fix_imageviewhelper');
