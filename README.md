Fix TYPO3 \<f:image\> ViewHelper
================================

Avoids enforcing width/height attribute on rendered _img_ tags.

This is especially useful, if you have a responsive website design which interferes with _img_ tag dimension attributes.

## Usage

Use the classic `f:image` ViewHelper from TYPO3 core. It is modified to no longer forcibly output the image dimensions
(_width_ and _height_ attribute of the resulting _img_ tag). 

The `f:image` ViewHelper gets an additional ViewHelper argument `(bool)` _usedimensions_: Set this to `true` to 
fall back to the default TYPO3 behaviour and output _width_ and _height_ attributes on the rendered _img_ tag. Defaults
to `false`, meaning these attributes are *not* rendered. 

## Features

* zero configuration, just install and done
* same VH namespace, no template adjustments
* minimal system impact

## Bugs

This Fix/Workaround has been used in different projects for now and are thoroughly real-life-tested. If there are any
remaining bugs: Please report them.
