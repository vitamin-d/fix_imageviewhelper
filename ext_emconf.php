<?php
$EM_CONF['fix_imageviewhelper'] = [
    'title' => 'Fix TYPO3 ImageViewHelper',
    'description' => 'Avoid enforcing width/height attribute on rendered tags',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'version' => '8.7.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
    ],
];
